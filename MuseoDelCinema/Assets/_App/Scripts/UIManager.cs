﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    public ImageSearchPage imageSearchPageIta;

    public ImageSearchPage imageSearchPageEng;

    public static UIManager Instance;

    [HideInInspector]
    public APage activePage;

    //all pages here


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else
        {
            Destroy(this);
        }
    }


    public void DisableSearchImage()
    {
        switch (AppFlowManager.Instance.currentLanguage)
        {
            case Language.Default:
                break;
            case Language.Italian:
                imageSearchPageIta.gameObject.SetActive(false);
                break;
            case Language.English:
                imageSearchPageEng.gameObject.SetActive(false);
                break;
            case Language.French:
                break;
            case Language.Spanish:
                break;
        }
    }


}
