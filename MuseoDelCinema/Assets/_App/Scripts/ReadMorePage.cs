﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ReadMorePage : APage
{

    public Image trasparencyImage;

    public TextMeshProUGUI trophyName;

    public TextMeshProUGUI longDescription;

    public void OpenURL(string link)
    {
        Application.OpenURL(link);
    }
}
