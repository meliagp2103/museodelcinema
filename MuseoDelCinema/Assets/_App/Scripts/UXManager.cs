﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public static class UXManager
{

    public static float transitionTime;
    public static float animationTime;
    public static float popUpAnimationTime;


    private static float progressionLerp1;
    private static float progressionLerp2;
    private static float progressionLerp3;

    private static APage currentPage;

    private static Vector2 pageOrigin;
    private static Vector2 pageDestination;
    private static Vector2 nextPageOrigin;
    private static Vector2 nextPageDestination;

    private static List<object> animationProcessedList = new List<object>();

    public static bool isProcessing => animationProcessedList.Count > 0;

    public static async void BeginPageAnimation(this APage nextPage)
    {
        if (progressionLerp1 != 0)
            return;

        animationProcessedList.Add(nextPage);

        currentPage = UIManager.Instance.activePage;

        pageOrigin = currentPage.rectTransform.anchoredPosition;

        nextPageOrigin = nextPage.rectTransform.anchoredPosition;

        float deltaX = Mathf.Abs(nextPageOrigin.x - pageOrigin.x);

        float deltaY = Mathf.Abs(nextPageOrigin.y - pageOrigin.y);

        if(deltaX > deltaY)
        {
            pageDestination = pageOrigin + (nextPageOrigin.x > pageOrigin.x ? Vector2.left : Vector2.right) * currentPage.rectTransform.rect.width;
        }

        else
        {
            pageDestination = pageOrigin + (nextPageOrigin.y > pageOrigin.y ? Vector2.down : Vector2.up) * currentPage.rectTransform.rect.height;
        }

        nextPageDestination = pageOrigin;

        nextPage.Activate();


        while(progressionLerp1 < 1)
        {
            progressionLerp1 += Time.deltaTime / transitionTime;

            if (deltaX > deltaY)
            {
                currentPage.rectTransform.anchoredPosition = new Vector2(Mathf.SmoothStep(pageOrigin.x,pageDestination.x,progressionLerp1),0);
                nextPage.rectTransform.anchoredPosition = new Vector2(Mathf.SmoothStep(nextPageOrigin.x,nextPageDestination.x,progressionLerp1),0);
            }

            else
            {
                currentPage.rectTransform.anchoredPosition = new Vector2(0,Mathf.SmoothStep(pageOrigin.y, pageDestination.y, progressionLerp1));
                nextPage.rectTransform.anchoredPosition = new Vector2(0,Mathf.SmoothStep(nextPageOrigin.y, nextPageDestination.y, progressionLerp1));
            }

            await Task.Yield();
        }

    }


}
