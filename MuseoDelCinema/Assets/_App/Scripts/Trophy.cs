﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trophy : MonoBehaviour
{

    public string hintText;

    public string trophyNameText;

    public string readMoreTitle;

    public string shortDescription;

    public string longDescription;

    public Sprite activeImage;

    public Sprite disabledImage;

    public Sprite readMoreImage;

    public Image buttonImageIta;

    public Image buttonImageEng;

    public bool hasFound;


    public void ChangeButtonImage()
    {
        switch (AppFlowManager.Instance.currentLanguage)
        {
            case Language.Default:
                break;
            case Language.Italian:
                buttonImageIta.sprite = activeImage;
                break;
            case Language.English:
                buttonImageEng.sprite = activeImage;
                break;
            case Language.French:
                break;
            case Language.Spanish:
                break;
        }
       
    }
}
