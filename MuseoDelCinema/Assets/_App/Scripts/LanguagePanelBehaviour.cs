﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguagePanelBehaviour : APage
{
    public void LanguageIta()
    {
        AppFlowManager.Instance.currentLanguage = Language.Italian;
    }

    public void LanguageEng()
    {
        AppFlowManager.Instance.currentLanguage = Language.English;
    }
}
