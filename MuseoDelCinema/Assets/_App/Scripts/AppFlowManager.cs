﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public enum AppStates
{
    LanguageSelectionState,
    TrophyState,
    PlanimetryState,
    ReservationState
}

public enum Language
{
    Default,
    Italian,
    English,
    French,
    Spanish
}

public class AppFlowManager : MonoBehaviour
{

    public AppStates currentAppState = AppStates.LanguageSelectionState;

    public Language currentLanguage = Language.Default;

    public static Action<AppStates> showCurrentPage;

    public static AppFlowManager Instance;

    public VuforiaBehaviour arCamera;

    public AppPanelPageBehaviour appPanelIta;

    public AppPanelPageBehaviour appPanelEng;

    public TrackableBehaviour target;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DisableCam();
        }

        else
        {
            Destroy(this);
        }
    }

    public void ChangeState(AppStates nextState)
    {
        currentAppState = nextState;

        showCurrentPage?.Invoke(currentAppState);
    }

    public void ChangeLanguage(Language nextLanguage)
    {
        currentLanguage = nextLanguage;
    }

    public void DisableCam()
    {
        arCamera.enabled = false;
    }


    public void EnableCam()
    {
        arCamera.enabled = true;
    }


    public void ChangeTrophyPageState(bool isActive)
    {

        Debug.Log("Invoked");

        switch (currentLanguage)
        {
            case Language.Default:
                break;
            case Language.Italian:

                appPanelIta.LoadBackGroundImage();

                DisableCam();

                if (isActive)
                {
                    appPanelIta.trophyPage.disabledPage.SetActive(false);

                    appPanelIta.trophyPage.enabledPage.SetActive(true);

                    appPanelIta.trophyPage.currentTrophy.ChangeButtonImage();

                    appPanelIta.trophyPage.LoadEnabledTrophyData();
                }

                else
                {

                    appPanelIta.trophyPage.disabledPage.SetActive(true);

                    appPanelIta.trophyPage.enabledPage.SetActive(false);

                    appPanelIta.trophyPage.LoadDisabledTrophyData();
                }
                break;
            case Language.English:

                appPanelEng.LoadBackGroundImage();

                DisableCam();

                if (isActive)
                {
                    appPanelEng.trophyPage.disabledPage.SetActive(false);

                    appPanelEng.trophyPage.enabledPage.SetActive(true);

                    appPanelEng.trophyPage.currentTrophy.ChangeButtonImage();

                    appPanelEng.trophyPage.LoadEnabledTrophyData();
                }

                else
                {

                    appPanelEng.trophyPage.disabledPage.SetActive(true);

                    appPanelEng.trophyPage.enabledPage.SetActive(false);

                    appPanelEng.trophyPage.LoadDisabledTrophyData();
                }
                break;
            case Language.French:
                break;
            case Language.Spanish:
                break;
        }
        
    }
}
