﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class TrophyPageBehaviour : APage
{

    public ReadMorePage readMorePage;

    public AppPanelPageBehaviour appPanelPage;

    public GameObject enabledPage;

    public GameObject disabledPage;

    public GameObject backgroundBanner;

    [HideInInspector]
    public Trophy currentTrophy;

    public TextMeshProUGUI trophyName;

    public TextMeshProUGUI trophyHintText;

    public TextMeshProUGUI trophyShortDescription;

    public Image trophyDisabledImage;

    public Image trophyEnabledImage;


    public void LoadEnabledTrophyData()
    {
        backgroundBanner.SetActive(true);

        disabledPage.SetActive(false);

        enabledPage.SetActive(true);

        currentTrophy.hasFound = true;

        trophyName.text = currentTrophy.trophyNameText;

        trophyShortDescription.text = currentTrophy.shortDescription;

        trophyEnabledImage.sprite = currentTrophy.activeImage;    
    }

    public void LoadReadMoreData()
    {
        readMorePage.trasparencyImage.sprite = currentTrophy.readMoreImage;

        readMorePage.trophyName.text = currentTrophy.readMoreTitle;

        readMorePage.longDescription.text = currentTrophy.longDescription;

    }

    public void LoadDisabledTrophyData()
    {
        if (currentTrophy.hasFound)
        {
            LoadEnabledTrophyData();

            return;
        }

        disabledPage.SetActive(true);

        backgroundBanner.SetActive(true);

        enabledPage.SetActive(false);

        trophyHintText.text = currentTrophy.hintText;

        trophyDisabledImage.sprite = currentTrophy.disabledImage;
    }


    public void SearchImage()
    {
        appPanelPage.UnLoadBackgroundImage();

        disabledPage.SetActive(false);

        enabledPage.SetActive(false);

        backgroundBanner.SetActive(false);

        AppFlowManager.Instance.EnableCam();
    }

    public void SetCurrentTrophy(Trophy trophyToLoad)
    {
        //Debug.Log(trophyToLoad);
        currentTrophy = trophyToLoad;
    }

}
