﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppPanelPageBehaviour : APage
{

    public TrophyPageBehaviour trophyPage;

    public Image backGroundImage;

    public void OpenURL(string link)
    {
        Application.OpenURL(link);
    }

    public void LoadTrophy(Trophy trophyToSet)
    {
        trophyPage.SetCurrentTrophy(trophyToSet);
        trophyPage.LoadDisabledTrophyData();
    }

    public void UnLoadBackgroundImage()
    {
        backGroundImage.enabled = false;
    }

    public void LoadBackGroundImage()
    {
        backGroundImage.enabled = true;
    }
}
