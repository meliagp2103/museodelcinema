﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APage : MonoBehaviour
{
    public RectTransform rectTransform;


    public void Activate()
    {
        UIManager.Instance.activePage = this;
        gameObject.SetActive(true);
    }
}
